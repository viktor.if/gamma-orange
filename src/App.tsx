import 'react-native-gesture-handler';
import React, { useState, useRef, useEffect } from 'react';
import { LogBox, Platform, StatusBar, StyleSheet } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import {
  EventOnAddStream,
  EventOnCandidate,
  MediaStream,
  RTCIceCandidate,
  RTCPeerConnection,
  RTCSessionDescription,
} from 'react-native-webrtc';

import firestore, {
  FirebaseFirestoreTypes,
} from '@react-native-firebase/firestore';

import IncomingCall from './components/IncomingCall';
import OnCall from './components/OnCall';
import StartCall from './components/StartCall';
import WebRTCHelper from './utils/WebRTCHelper';
import ignoreWarningsList from '../src/utils/ignoreWarningsList';

import {
  ADDED,
  CALLEE,
  CALLER,
  CHAT_ID,
  MEET,
  REMOVED,
} from './utils/constants';

LogBox.ignoreLogs(ignoreWarningsList);

const configuration = { iceServers: [{ url: 'stun:stun.l.google.com:19302' }] };

export default () => {
  const [localStream, setLocalStream] = useState<MediaStream | null>(null);
  const [remoteStream, setRemoteStream] = useState<MediaStream | null>(null);
  const [isIncomingCall, setIsIncomingCall] = useState<boolean>(false);
  const pc = useRef<RTCPeerConnection>();
  const isConnecting = useRef<boolean>(false);

  /**
   * Set up WebRTC connection
   */
  const setupWebRTC = async () => {
    try {
      pc.current = new RTCPeerConnection(configuration);

      // Get audio and video streams
      const stream = await WebRTCHelper.getStream();

      if (stream) {
        setLocalStream(stream);
        pc.current.addStream(stream);
      }

      // Set up remote stream once it is available
      pc.current.onaddstream = (event: EventOnAddStream) => {
        setRemoteStream(event.stream);
      };
    } catch (error) {
      console.warn(error);
    }
  };

  /**
   * Make a call
   */
  const call = async () => {
    console.log('=== Call ===');

    try {
      isConnecting.current = true;
      await setupWebRTC();

      const docRef = firestore().collection(MEET).doc(CHAT_ID);

      await exchangeIceCandidate(docRef, CALLER, CALLEE);

      if (pc.current) {
        // Create offer
        const offer = await pc.current.createOffer();
        pc.current.setLocalDescription(offer);

        const docWithOffer = {
          offer: {
            type: offer.type,
            sdp: offer.sdp,
          },
        };

        // Store offer at docs
        docRef.set(docWithOffer);
      }
    } catch (error) {
      console.warn(error);
    }
  };

  /*
   * Join a call
   */
  const join = async () => {
    console.log('=== Join ===');

    try {
      isConnecting.current = true;
      setIsIncomingCall(false);

      const cRef = firestore().collection(MEET).doc(CHAT_ID);
      const offer = (await cRef.get()).data()?.offer;

      if (offer) {
        await setupWebRTC();
        await exchangeIceCandidate(cRef, CALLEE, CALLER);

        if (pc.current) {
          pc.current.setRemoteDescription(new RTCSessionDescription(offer));

          // Create answer
          const answer = await pc.current.createAnswer();
          pc.current.setLocalDescription(answer);

          const docWithAnswer = {
            answer: {
              type: answer.type,
              sdp: answer.sdp,
            },
          };

          cRef.update(docWithAnswer);
        }
      }
    } catch (error) {
      console.warn(error);
    }
  };

  /**
   * Hang up
   * To close connection, release the stream and delete doc
   */
  const hangup = async () => {
    console.log('=== Hang up ===');

    try {
      setIsIncomingCall(false);
      isConnecting.current = false;

      if (pc.current) {
        pc.current.close();
      }

      await streamCleanUp();
      await firestoreCleanUp();
    } catch (error) {
      console.warn(error);
    }
  };

  /**
   * Release local stream
   */
  const streamCleanUp = () => {
    if (localStream) {
      localStream.getTracks().forEach((track) => track.stop());
      localStream.release();
    }

    setLocalStream(null);
    setRemoteStream(null);
  };

  /**
   * Clear doc
   */
  const firestoreCleanUp = async () => {
    try {
      const docRef = firestore().collection(MEET).doc(CHAT_ID);

      if (docRef) {
        const calleeCandidate = await docRef.collection(CALLEE).get();
        calleeCandidate.forEach(async (candidate) => {
          await candidate.ref.delete();
        });

        const callerCandidate = await docRef.collection(CALLER).get();
        callerCandidate.forEach(async (candidate) => {
          await candidate.ref.delete();
        });

        docRef.delete();
      }
    } catch (error) {
      console.warn(error);
    }
  };

  /**
   * Exchange ICE candidates between the caller and callee
   */
  const exchangeIceCandidate = (
    docRef: FirebaseFirestoreTypes.DocumentReference<FirebaseFirestoreTypes.DocumentData>,
    localName: string,
    remoteName: string,
  ) => {
    const candidateCollection = docRef.collection(localName);

    if (pc.current) {
      // On new ICE candidate is added to the firestore
      pc.current.onicecandidate = (event: EventOnCandidate) => {
        if (event.candidate) {
          candidateCollection.add(event.candidate);
        }
      };
    }

    // Get the ICE candidate added to firestore and update the local PC
    docRef.collection(remoteName).onSnapshot((snapshot) => {
      snapshot.docChanges().forEach(async (change: any) => {
        if (change.type === ADDED) {
          try {
            const candidate = new RTCIceCandidate(change.doc.data());
            await pc.current?.addIceCandidate(candidate);
          } catch (error) {
            console.warn(error);
          }
        }
      });
    });
  };

  useEffect(() => {
    // Get document ref
    const docRef = firestore().collection('meet').doc('chatId');

    // Listen changes to a document
    const stopListeningChatId = docRef.onSnapshot(async (snapshot) => {
      const data = snapshot.data();

      // Start a call on answer
      if (pc.current && !pc.current.remoteDescription && data && data.answer) {
        try {
          await pc.current.setRemoteDescription(
            new RTCSessionDescription(data.answer),
          );
        } catch (error) {
          console.warn(error);
        }
      }

      // Set incoming flag if there is offer
      if (data && data.offer && !isConnecting.current) {
        setIsIncomingCall(true);
      }
    });

    // Delete collection if other side has clicked on hangup
    const stopListeningCallee = docRef
      .collection(CALLER)
      .onSnapshot((snapshot) => {
        snapshot.docChanges().forEach(async (change) => {
          try {
            console.log(change.type);
            if (change.type === REMOVED) {
              await hangup();
            }
          } catch (error) {
            console.warn(error);
          }
        });
      });

    return () => {
      // Remove listeners
      stopListeningChatId();
      stopListeningCallee();
    };
  }, []);

  useEffect(() => {
    StatusBar.setBarStyle('dark-content');

    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('#fff');
      StatusBar.setTranslucent(true);
    }
  }, []);

  if (isIncomingCall) {
    return (
      <SafeAreaView style={styles.screen}>
        <StatusBar translucent />
        <IncomingCall hangup={hangup} join={join} />
      </SafeAreaView>
    );
  }

  if (localStream) {
    return (
      <SafeAreaView style={styles.screen}>
        <StatusBar translucent />
        <OnCall
          hangup={hangup}
          localStream={localStream}
          remoteStream={remoteStream}
        />
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar translucent barStyle="dark-content" backgroundColor="#fff" />
      <StartCall call={call} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
  },
});
