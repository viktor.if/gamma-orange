import React from 'react';
import {
  StyleProp,
  ViewStyle,
  GestureResponderEvent,
  TouchableOpacity,
} from 'react-native';

import styles from './styles';

type ButtonRoundProps = {
  backgroundColor: string;
  containerStyle?: StyleProp<ViewStyle>;
  onPress?: (event: GestureResponderEvent) => void;
  icon?: JSX.Element;
};

export default ({
  backgroundColor,
  containerStyle,
  icon,
  onPress,
}: ButtonRoundProps) => (
  <TouchableOpacity
    {...{ onPress }}
    style={[styles.container, { backgroundColor }, containerStyle]}
  >
    {icon}
  </TouchableOpacity>
);
