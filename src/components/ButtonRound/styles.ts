import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    alignSelf: 'center',
    borderRadius: 1000,
    flex: 0,
    margin: 32,
    padding: 16,
  },
});
