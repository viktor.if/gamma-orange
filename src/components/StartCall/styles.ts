import { StyleSheet } from 'react-native';
import { screenWidth } from '../../utils/styles';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: screenWidth,
  },
  topSubContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    color: '#000',
    fontSize: 28,
    textTransform: 'uppercase',
    fontWeight: '500',
  },
  bottomSubContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});
