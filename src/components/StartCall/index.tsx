import React from 'react';
import { View, Text } from 'react-native';
import ButtonRound from '../ButtonRound';
import PhoneInTalkIcon from '../../../assets/svgs/phone-in-talk.svg';
import styles from './styles';

type StartCallProps = {
  call: () => void;
};

export default ({ call }: StartCallProps) => (
  <View style={styles.container}>
    <View style={styles.topSubContainer}>
      <Text style={styles.text}>Call</Text>
    </View>
    <View style={styles.bottomSubContainer}>
      <ButtonRound
        icon={<PhoneInTalkIcon fill="#fff" />}
        backgroundColor="#109406"
        onPress={call}
      />
    </View>
  </View>
);
