import React from 'react';
import { View } from 'react-native';
import { MediaStream, RTCView } from 'react-native-webrtc';

import ButtonRound from '../ButtonRound';
import PhoneHangupIcon from '../../../assets/svgs/phone-hangup.svg';
import styles from './styles';

type OnCallProps = {
  hangup: () => void;
  localStream: MediaStream | null;
  remoteStream: MediaStream | null;
};

export default ({ hangup, localStream, remoteStream }: OnCallProps) => {
  /** Outcoming call */
  if (localStream && !remoteStream) {
    return (
      <View style={styles.constainer}>
        <RTCView
          streamURL={localStream.toURL()}
          objectFit="cover"
          style={styles.video}
        />
        <ButtonRound
          backgroundColor="#fe0107"
          icon={<PhoneHangupIcon fill="#fff" />}
          onPress={hangup}
        />
      </View>
    );
  }

  /** On call */
  if (localStream && remoteStream) {
    return (
      <View style={styles.constainer}>
        <RTCView
          streamURL={remoteStream.toURL()}
          objectFit="cover"
          style={styles.video}
        />
        <RTCView
          streamURL={localStream.toURL()}
          objectFit="cover"
          style={styles.videoLocal}
        />
        <ButtonRound
          backgroundColor="#fe0107"
          icon={<PhoneHangupIcon fill="#fff" />}
          onPress={hangup}
        />
      </View>
    );
  }

  return null;
};
