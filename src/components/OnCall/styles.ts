import { StyleSheet } from 'react-native';
import { screenWidth } from '../../utils/styles';

export default StyleSheet.create({
  constainer: {
    flex: 1,
    width: screenWidth,
    justifyContent: 'flex-end',
  },
  video: {
    ...StyleSheet.absoluteFillObject,
  },
  videoLocal: {
    borderRadius: 6,
    height: 150,
    left: 20,
    position: 'absolute',
    top: 20,
    width: 100,
    overflow: 'hidden',
  },
});
