import React from 'react';
import { View, Text } from 'react-native';
import ButtonRound from '../ButtonRound';
import PhoneIcon from '../../../assets/svgs/phone.svg';
import CloseIcon from '../../../assets/svgs/close.svg';
import styles from './styles';

type IncomingCallProps = {
  hangup: () => void;
  join: () => void;
};

export default ({ hangup, join }: IncomingCallProps) => (
  <View style={styles.container}>
    <View style={styles.topSubContainer}>
      <Text style={styles.text}>Incoming...</Text>
    </View>
    <View style={styles.bottomSubContainer}>
      <ButtonRound
        icon={<CloseIcon fill="#fff" />}
        backgroundColor="#fe0107"
        onPress={hangup}
      />
      <ButtonRound
        icon={<PhoneIcon fill="#fff" />}
        backgroundColor="#109406"
        onPress={join}
      />
    </View>
  </View>
);
