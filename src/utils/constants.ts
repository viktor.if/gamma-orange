export const CALLER = 'caller';
export const CALLEE = 'callee';
export const REMOVED = 'removed';
export const MEET = 'meet';
export const CHAT_ID = 'chatId';
export const ADDED = 'added';
