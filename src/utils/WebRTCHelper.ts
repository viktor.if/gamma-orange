import { mediaDevices } from 'react-native-webrtc';

export default class {
  static async getStream() {
    let isFront = true;

    try {
      /**
       * Request a list of the available media input and output devices,
       * such as microphones, cameras, headsets, and so forth
       */
      const sourceInfos = await mediaDevices.enumerateDevices();

      // Find front camera
      let videoSourceId;
      for (let i = 0; i < sourceInfos.length; i++) {
        const sourceInfo = sourceInfos[i];
        if (
          sourceInfo.kind === 'videoinput' &&
          sourceInfo.facing === (isFront ? 'front' : 'environment')
        ) {
          videoSourceId = sourceInfo.deviceId;
        }
      }

      /**
       * Prompts the user for permission to use a media input
       * which produces a MediaStream with tracks containing the requested types of media
       */
      const stream = await mediaDevices.getUserMedia({
        audio: true,
        video: {
          facingMode: isFront ? 'user' : 'environment',
          mandatory: {
            minHeight: 720,
            minWidth: 720,
            minFrameRate: 30,
          },
          optional: [videoSourceId],
        },
      });

      if (typeof stream !== 'boolean') {
        return stream;
      }
    } catch (error) {
      console.warn(error);
    }

    return null;
  }
}
